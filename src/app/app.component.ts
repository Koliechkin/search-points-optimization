import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  // CONFIG later input data
  // plurality: number[] = [1, 2, 3, 4, 5, 6, 7, 8]; // set of numbers
  plurality: number[] = [1, 2, 2, 3, 4, 4, 6, 6]; // set of numbers
  // plurality: number[] = [1, 2, 3, 4, 5, 6]; // set of numbers
  // plurality: number[] = [1, 2, 2, 4]; // set of numbers

  // conditions config
  conditions: Condition[] = [
    // {
    //   vector: [-5950, -2518, -2388, -1634, -594, 654, 7258, 8766],
    //   conditionPointVal: 0,
    //   conditionSign: "=="
    // },
    // {
    //   vector: [-4, -1, 1, 10, 2, 4, 7, 8],
    //   conditionPointVal: 95,
    //   conditionSign: "==" // <=
    // },
    {
      vector: [3, 5, -4, -5, -7, -1, -2, 4],
      conditionPointVal: -12,
      conditionSign: ConditionSigns.EQUAL // <=
    }
    // other tests conditions
    // {
    //   vector: [2, 3, 4, 6, 7, 8],
    //   conditionPointVal: 109,
    //   conditionSign: "==" // <=
    // }
    // {
    //   vector: [1, -2, 3, 4],
    //   conditionPointVal: 10,
    //   conditionSign: "<=" // <=
    // }
  ];

  // arrays
  readonly factorialArr: number[] = [];
  solutionPoints: { [key: string]: number[][] } = {};
  currentLevels: Level[] = [];
  nextLevels: Level[] = [];

  // restriction points
  maxValString: string;
  minValString: string;

  // helpers
  processTime: number = 0; // milliseconds
  currentConditionIndex: number; // index from condition array
  numberOfAnalizedPoints: number = 0;

  // flags
  isCounting: boolean = false;

  ngOnInit() {
    // this.MaxValue; // called here to avoid error ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked.
    // this.MinValue; // like above
  }

  start() {
    this.isCounting = true;
    let start = new Date().getTime();

    console.log("Start, d: ", this.timeLog());
    this.conditions.forEach((cond: Condition, i) => {
      this.currentConditionIndex = i;
      this.solutionPoints[`condition_${this.currentConditionIndex}`] = [];
      let startVector = this.conditions[i].vector.map(r => r);
      let newIndexes = [];
      // for return back reversed indexes
      console.log("startVector: ", startVector);
      this.conditions[i].vector.sort((a, b) => a - b);
      console.log("sorted vector: ", this.conditions[i].vector);
      console.warn("CONDITION index: ", this.currentConditionIndex);

      this.createLevels();

      // this.sortVector();
      console.warn("TODO reverse indexes after sorting");
      console.log(
        "Current condition solutions: ",
        this.solutionPoints[`condition_${i}`]
      );
    });
    console.log("Finish: ", this.timeLog());
    console.log("Solution points: ", this.solutionPoints);
    this.processTime = new Date().getTime() - start;
  }

  // todo (for test revert indexes)
  sortVector() {
    let startVector = [1, -2, 3, 4];
  }

  get FactorialArr() {
    return this.plurality.map(e => e);
  }

  /**
   * get goals function result for multiple every index of vector array on ASCending indexes of factorial array
   *
   * @readonly
   * @memberof TestComponent
   */
  getMaxValue(vector: number[]) {
    if (this.FactorialArr.length) {
      let maxV = 0;
      this.maxValString = "";
      vector.map(x => x).sort((a, b) => a - b);
      // console.log(vector)
      this.FactorialArr.forEach((el, i) => {
        maxV += el * vector[i];
        this.maxValString += `${el} * ${vector[i]} ${
          i < this.FactorialArr.length - 1 ? "+" : ""
        } `;
      });

      return maxV;
    }
  }

  /**
   * get goals function result for multiple every index of vector array on DESCending indexes of factorial array
   *
   * @readonly
   * @memberof TestComponent
   */
  getMinValue(vector: number[]) {
    if (this.FactorialArr.length) {
      let minV = 0;
      this.minValString = "";
      vector.map(x => x).sort((a, b) => a - b);
      this.FactorialArr.reverse().forEach((el, i) => {
        minV += el * vector[i];
        this.minValString += `${el} * ${vector[i]} ${
          i < this.FactorialArr.length - 1 ? "+" : ""
        } `;
      });
      return minV;
    }
  }

  getMaxForBlockedValue(indexes: number[]) {
    if (this.FactorialArr.length && indexes && indexes.length) {
      // remove no needed indexes
      let newArr = this.getPointWithBlockedValues(true, indexes);

      return this.getFromNewArrFunctionVal(newArr);
    }
  }

  getMinForBlockedValue(indexes: number[]) {
    if (this.FactorialArr.length && indexes && indexes.length) {
      // remove no needed indexes
      let newArr = this.getPointWithBlockedValues(false, indexes);

      return this.getFromNewArrFunctionVal(newArr);
    }
  }

  getFromNewArrFunctionVal(newArr) {
    let maxV = 0;
    newArr.forEach((el, i) => {
      maxV += el * this.conditions[this.currentConditionIndex].vector[i];
    });
    return maxV;
  }

  getPointWithBlockedValues(forMax: boolean, indexes: number[]) {
    // console.log('getPointWithBlockedIndexes: ', indexes, ' forMax ', forMax);
    let newArr = this.FactorialArr.filter((v, i) =>
      indexes.indexOf(i) === -1 ? v : false
    );

    if (!forMax) newArr.reverse();
    newArr.push(...this.getBlockedValues(indexes));
    // console.log('newArr ', newArr);
    return newArr;
  }

  // get levels
  createLevels(blockedIndexes?: number[]) {
    let levels: Level[] = []; // for current blocked values
    console.log(
      "CreateLevels() for blocked indexes: ",
      blockedIndexes ? blockedIndexes.toString() : "root"
    );
    console.log(blockedIndexes);
    this.FactorialArr.forEach((num: number, i) => {
      if (!blockedIndexes || blockedIndexes.indexOf(i) === -1) {
        let newLevel = new Level();
        newLevel.levelValues = blockedIndexes
          ? [num, ...this.getBlockedValues(blockedIndexes)]
          : [num];
        newLevel.blockedIndexes = blockedIndexes ? [i, ...blockedIndexes] : [i];
        newLevel.maxValue = this.getMaxForBlockedValue(newLevel.blockedIndexes);
        newLevel.maxPoint = this.getPointWithBlockedValues(
          true,
          newLevel.blockedIndexes
        );

        newLevel.minValue = this.getMinForBlockedValue(newLevel.blockedIndexes);
        newLevel.minPoint = this.getPointWithBlockedValues(
          false,
          newLevel.blockedIndexes
        );
        levels.push(newLevel);
      }
    });
    console.log(" levels: ", levels);

    if (levels.length) this.makeLevelsAnalize(levels);
  }

  getBlockedValues(indexes: number[]) {
    return indexes.map(v => {
      return this.FactorialArr[v];
    });
  }

  makeLevelsAnalize(levels: Level[]) {
    // console.log("Analize levels: ", levels);
    levels.forEach((level: Level) => {
      this.numberOfAnalizedPoints += 2;

      switch (this.conditions[this.currentConditionIndex].conditionSign) {
        case ConditionSigns.EQUAL:
          this.equalCondition(level);
          break;

        case ConditionSigns.LESS_EQUAL:
          this.lessEqualCondition(level);
          break;

        default:
          break;
      }
    });
  }

  // When <=
  lessEqualCondition(level: Level) {
    console.log("lessEqualCondition()");
    if (
      level.maxValue ===
        this.conditions[this.currentConditionIndex].conditionPointVal ||
      level.maxValue <=
        this.conditions[this.currentConditionIndex].conditionPointVal
    )
      this.putSolutionPoint(level.maxPoint);

    if (
      level.minValue ===
        this.conditions[this.currentConditionIndex].conditionPointVal ||
      level.minValue <=
        this.conditions[this.currentConditionIndex].conditionPointVal
    )
      this.putSolutionPoint(level.minPoint);

    if (
      level.minValue <
      this.conditions[this.currentConditionIndex].conditionPointVal
    ) {
      console.log("NEXT levels creation! ");
      this.createLevels(level.blockedIndexes);
    }
  }

  // when === condition
  equalCondition(level: Level) {
    // console.log("equalCondition()");
    if (
      level.maxValue ===
      this.conditions[this.currentConditionIndex].conditionPointVal
    ) {
      this.putSolutionPoint(level.maxPoint);
    }

    if (
      level.minValue ===
      this.conditions[this.currentConditionIndex].conditionPointVal
    ) {
      this.putSolutionPoint(level.minPoint);
    }

    if (
      level.minValue <
        this.conditions[this.currentConditionIndex].conditionPointVal &&
      level.maxValue >
        this.conditions[this.currentConditionIndex].conditionPointVal &&
      level.minValue !== level.maxValue
    ) {
      console.log(
        "NEXT levels creation!  min val < condition || maxval > condition point "
      );
      this.createLevels(level.blockedIndexes);
    }
  }

  putSolutionPoint(point: number[]) {
    console.warn("PUSH POINT! ", point);
    if (
      !this.solutionPoints[`condition_${this.currentConditionIndex}`].filter(
        (_point: number[]) => {
          if (this.checkIfArrayIdentical(_point, point)) return _point;
        }
      ).length
    )
      this.solutionPoints[`condition_${this.currentConditionIndex}`].push(
        point
      );
  }

  checkIfArrayIdentical(arr1, arr2) {
    if (arr1.length !== arr2.length) return false;
    for (var i = arr1.length; i--; ) {
      if (arr1[i] !== arr2[i]) return false;
    }

    return true;
  }

  getNumberOfPoints() {
    // return n ? n * this.getNumberOfPoints(n - 1) : 1;
    return this.plurality.reduce((prevVal: number, curVal: number) => {
      return prevVal * curVal;
    });
  }

  iterateSolutions() {
    let arr = [];
    Object.keys(this.solutionPoints).forEach((key, i) => {
      arr.push(this.solutionPoints[key]);
    });
    return arr;
  }

  timeLog() {
    return `[${new Date().toLocaleDateString() +
      " " +
      new Date().toLocaleTimeString()}]`;
  }
}

export class Level {
  levelValues: number[]; // blocked values
  blockedIndexes: number[]; // needed for repeated values
  maxPoint: number[];
  minPoint: number[];
  maxValue: number;
  minValue: number;
}

export class Condition {
  vector: number[];
  conditionPointVal: number;
  conditionSign: string; // >,<,<=, >=, ===
}

export enum ConditionSigns {
  EQUAL = "==",
  LESS_EQUAL = "<="
}
